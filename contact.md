---
layout: goback
title: "Contact"
colors:
  stroke: '#0e738e'
  light: '#92fe9d'
  dark: '#0dc1f2'
  buttline: '#00a5d0'
description: "Join the Burnaby South Physics and Senior Math Club online or in real life."
---
# discord.gg/dAySx9N

Our Discord server has plenty of room for Klein bottle discussion, respectful banter, and students willing to give homework advice.

Upon entering the server, please change your nickname to something recognizeable by other students, preferably the name you use in school.

New members who haven't visited the club in person are allowed text messages but nothing else. You get more permissions if you show a genuine interest in constructive communication with server members. Coming to the club in person with a positive attitude gets you on the trusted list quicker!


