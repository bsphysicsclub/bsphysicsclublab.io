# Burnaby South Physics and Senior Math Club

[spasm.club](https://spasm.club)

This is the source code for the official website of SPaSM Club, the Burnaby South Physics and Senior Math Club. Give it a look if you haven't already; we're awfully proud of it.

## Contributing

If you would like, you may contribute to this website. You don't even have to be in the club, but if this website is your kind of thing then you would probably enjoy the club as well. 

You must have a GitLab account in order to contribute. Create a fork of this repository under your account and make your changes there. Then create a merge request. [Here are some instructions.](https://docs.gitlab.com/ee/workflow/forking_workflow.html)

We won't be critical. We really want to accept your contributions and make this website a collaborative work. But we may edit your contribution a bit. 

Instructions for specifically contributing to the blog are below, for those who just want to write. If you want to help with development, we'll assume you know what you're doing and you can figure it out.

## Making a Blog Post

Blog posts must be placed in the directory `blog/_posts`. 

The filename of the post should be in the format `YYYY-MM-DD-your-post-title.markdown`, where the date is the date that you wrote the post and the rest of the filename is your post title with spaces replaced by hyphens. 

We will edit the filename behind your back if you do not format it properly. 

The file must begin with this header: 

```
---
layout: post
title: "YOUR POST TITLE"
date: YYYY-MM-DD HH:MM:SS
author: "YOUR NICKNAME"
---
```

Again, we will edit this part at our discretion if you do not format it properly or omit it.

Write the content of your post underneath the header. 

Posts are written in a Markdown variant. There is some documentation [here](https://daringfireball.net/projects/markdown/syntax#block), but we officially use [kramdown](https://kramdown.gettalong.org/). This probably doesn't matter unless you want to use kramdown's advanced features. 

Writing the post in HTML is acceptable, too.

If you make a few syntax errors, we will fix them silently.

There are no other guidelines for writing posts, but please put effort into them. We prefer posts with proper grammar and style, but you may forgo that for artistic reasons at your discretion. *Be reasonable.*

Please also be consistent with your nickname. If you want to change it, please change all instances of your nickname throughout the website.

## Technical Contributions 

This website runs on [Jekyll](https://jekyllrb.com/). We trust you can figure it out (but feel free to ask if you want to know how something works). 

Responsiveness and accessibility of the site is a *must*, and your contribution will be rejected if it doesn't have those features.

Not sure where to start? Here are some things on the to-do list:

- Improve the design of the Klein bottle generator
- Fix the bug where the generator outputs a blank white PNG on Firefox

## Contact

If you have any questions, feel free to ask in our Discord server!
