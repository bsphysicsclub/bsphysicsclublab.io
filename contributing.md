---
layout: post
title: "Contributing"
colors:
  stroke: '#9b096a'
  light: '#f7ff00'
  dark: '#db36a4'
  buttline: '#a2096f'
description: "How to contribute to this website."
---
This blog accepts submissions from anyone.

The source code of this website is hosted in a public [repository](https://gitlab.com/bsphysicsclub/bsphysicsclub.gitlab.io) on GitLab. Just fork the project, add your own post, and submit a merge request! More detailed instructions are available in the README.

Anyone is free to contribute to the blog, but you must make a GitLab account. 
