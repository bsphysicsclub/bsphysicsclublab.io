---
layout: post
title: "This better not be yet another overly-ambitious project."
date: 2018-09-15 22:44:00
author: Madmin 
---
I bet you were expecting a math blog.

In a sense, this is one. It is a math blog in the sense that we will be posting about math. It is *not* a math blog in the sense that we will *only* be posting about math. It is a math blog in the sense that we will find the beauty in *anything* and *everything*. It is *not* a math blog in the sense that we know what we are talking about or that everything we say will make sense.

What you might not know is that the main writer of this website is currently attending the University of Waterloo, not Burnaby South.

What you might not know is that the main writer of this website is studying computer science to be a novelist, not a programmer.

In any case, the world does hold many such contradictions and they may sometimes erupt from underneath. Should this occur, hold on tight. You'll see your regularly schedule math programming soon.

This is an experiment. Let's hope it lasts. Is anybody interested in learning proofs? 
