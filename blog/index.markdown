---
layout: default
title: Burnaby South Physics and Senior Math Blog
colors:
  stroke: '#9b096a'
  light: '#f7ff00'
  dark: '#db36a4'
  buttline: '#a2096f'
style: blog
description: "The best Burnaby South blog in the entire universe. Posts on real things from real people."
---
<div class="content">
{% include website-title.html %}

<h1>Have it in writing.</h1>

<p>The best Burnaby South blog in the entire universe. Posts on real things from real people. Interesting findings. Extended navel-gazing. And of course, updates and events from your favourite club of all time.</p>

<div class="postbar">
{% include post-list.html %}
</div>

<footer>
{% include nav-blog.html %}
</footer>

</div>
