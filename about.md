---
layout: goback
title: "About"
colors:
  dark: '#ef32d9'
  light: '#89fffd'
  stroke: '#920781'
  buttline: '#e705cc'
description: "Burnaby South Physics and Senior Math Club is a club for everyone, not just the math-inclined."
---
# Today is not your special day.

In the beginning, there was the physics club and the senior math club. And all was good.

Then all the seniors were gone. 

We became <span class="spasm">SPaSM</span>:  
(Burnaby) South's Physics and Senior Math Club. 

Please don't blame us. We needed somewhere to go:  
*Thursdays after school at A214, Mr. Lock's room.* 

Are You Lonely? Are You Bored?  
Struggling With Homework Or Studies?  

We're here for all of that and more. And sometimes we build keyboards. Everyone has a place here, even business students. 
